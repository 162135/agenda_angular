import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserInsertRoutingModule } from './user-insert-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    UserInsertRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserInsertModule { }
