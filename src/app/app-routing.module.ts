import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './shared/components/nav/nav.component';
import { AddUserComponent } from './user/user-insert/components/add-user/add-user.component';

const routes: Routes = [
  {
    path: '',
    component: NavComponent,
    children: [
      {
        path: 'activity',
        loadChildren: () =>
          import('./activity/activity-insert/activity-insert.module').then(
            (m) => m.ActivityInsertModule
          ),
      },
    ],
  },
  {
    path: '',
    component: NavComponent,
    children: [
      {
        path: 'contact',
        loadChildren: () =>
          import('./contact/contact-insert/contact-insert.module').then(
            (m) => m.ContactInsertModule
          ),
      },
    ],
  },
  {
    path: 'user',
    loadChildren: () =>
      import('./user/user-insert/user-insert.module').then(
        (m) => m.UserInsertModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
